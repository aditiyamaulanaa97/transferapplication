package com.AditiyaMaulana.TransferApp.entity;

import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name="nasabah")
public class Nasabah {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@NotEmpty
	@NotBlank
	@NotNull
	@Size(min=5, max=30)
	private String namaLengkap;
	private Date tglLahir;
	@NotEmpty
	@NotBlank
	@NotNull
	private String noIdentitas;
	@NotEmpty
	@NotBlank
	@NotNull
	private String tipeIdentitas;
	@NotEmpty
	@NotBlank
	@NotNull
	@Email
	private String email;
	@NotEmpty
	@NotBlank
	@NotNull
	private String noContact;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "nasabah", cascade = CascadeType.ALL)
	private List<Rekening> listRekening;
	
	
	public String getTipeIdentitas() {
		return tipeIdentitas;
	}
	public void setTipeIdentitas(String tipeIdentitas) {
		this.tipeIdentitas = tipeIdentitas;
	}
	public List<Rekening> getListRekening() {
		return listRekening;
	}
	public void setListRekening(List<Rekening> listRekening) {
		this.listRekening = listRekening;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNamaLengkap() {
		return namaLengkap;
	}
	public void setNamaLengkap(String namaLengkap) {
		this.namaLengkap = namaLengkap;
	}
	public Date getTglLahir() {
		return tglLahir;
	}
	public void setTglLahir(Date tglLahir) {
		this.tglLahir = tglLahir;
	}
	public String getNoIdentitas() {
		return noIdentitas;
	}
	public void setNoIdentitas(String noIdentitas) {
		this.noIdentitas = noIdentitas;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNoContact() {
		return noContact;
	}
	public void setNoContact(String noContact) {
		this.noContact = noContact;
	}
	
	
}
