package com.AditiyaMaulana.TransferApp.controller;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.AditiyaMaulana.TransferApp.entity.HistoryAmount;
import com.AditiyaMaulana.TransferApp.entity.Provider;
import com.AditiyaMaulana.TransferApp.entity.Rekening;
import com.AditiyaMaulana.TransferApp.entity.Transfer;
import com.AditiyaMaulana.TransferApp.service.AdminService;
import com.AditiyaMaulana.TransferApp.service.HistoryService;
import com.AditiyaMaulana.TransferApp.service.RekeningService;
import com.AditiyaMaulana.TransferApp.service.TransferService;

@Controller
@RequestMapping("/operator")
public class OperatorController {

	@Autowired
	private TransferService transferService;
	
	@Autowired
	private RekeningService rekeningService;
	
	@Autowired
	private AdminService adminService;
	
	@Autowired
	private HistoryService historyService;
	
	@GetMapping
	public String index(@RequestParam(required = false) String keyword,
			@RequestParam(value = "pageNo", defaultValue = "0") int pageNo,
			@RequestParam(value = "pageSize", defaultValue = "1") int pageSize,
			@RequestParam(value = "sortField", defaultValue = "id") String sortField,Model model) {
		Page<HistoryAmount> listHistory;
		Page<Transfer> listTransfer;
		List<Transfer> listTransfers;
		List<Rekening> listRekening = this.rekeningService.getAll();
		if(keyword == null) {
			listTransfer = this.transferService.getAllPaginate(pageNo, pageSize, sortField);
		}else {
			listTransfer = this.transferService.getPaginateSearch(pageNo, pageSize, sortField, keyword);
		}
		listTransfers = this.transferService.getAll();
		model.addAttribute("transferForm", new Transfer());
		model.addAttribute("historyForm", new HistoryAmount());
		model.addAttribute("keyword", keyword);
		model.addAttribute("rekenings", listRekening);
		model.addAttribute("page", listTransfers);
		return "operator";
		
	}
	
	@PostMapping("/save")
	public String save(@Valid @ModelAttribute("transferForm") Transfer transfer,@Valid @ModelAttribute("historyForm") HistoryAmount historyAmount,@RequestParam(value = "pageNo", defaultValue = "0") int pageNo,
			@RequestParam(value = "pageSize", defaultValue = "1") int pageSize,
			@RequestParam(value = "sortField", defaultValue = "id") String sortField,BindingResult result,
			RedirectAttributes redirectAttributes, Model model) {
		Transfer tran = transfer;
		Page<Transfer> listTransfer = this.transferService.getAllPaginate(pageNo, pageSize, sortField);
		List<Rekening> listRek = this.rekeningService.getAll();
		
		if(result.hasErrors()) {
			model.addAttribute("page", listTransfer);
			return "operatorTransaction";
		}
		else {
			Rekening rekeningPengirim = rekeningService.findByNoRekening(tran.getRekening_pengirim().getNoRekening());
			Rekening rekeningPenerima = rekeningService.findByNoRekening(tran.getRekening_penerima().getNoRekening());
			historyAmount.setRekeningPenerima(tran.getRekening_penerima().getNoRekening());
			historyAmount.setRekeningPengirim(tran.getRekening_pengirim().getNoRekening());
			historyAmount.setTanggalKirim(Date.valueOf(LocalDate.now()));
			Long idPengirim = rekeningPengirim.getId();
			Long idPenerima = rekeningPenerima.getId();
			if(idPengirim == null || idPenerima == null) {
				model.addAttribute("transfer", listTransfer);
				redirectAttributes.addFlashAttribute("Fail", "Rekening Pengirim atau Penerima tidak ada");
				return "redirect:/operator";
			}
			else if(idPengirim!=null && idPenerima!=null) {
				Optional<Rekening> pengirim = this.rekeningService.getRekeningId(idPengirim);
				Optional<Rekening> penerima = this.rekeningService.getRekeningId(idPenerima);
				Rekening rekPengirim = pengirim.get();
				Rekening rekPenerima = penerima.get();
				String bankAsal = rekPengirim.getProvider().getNamaBank();
				String bankTuju = rekPenerima.getProvider().getNamaBank();
				double amount = tran.getAmount();
				double sisa;
				if(bankAsal.equals(bankTuju)) {
					sisa = tran.getRekening_pengirim().getSaldo() - amount;
					if(sisa < 50000) {
						model.addAttribute("transfer", listTransfer);
						redirectAttributes.addFlashAttribute("Fail", "Saldo kurang dari 50000");
						return "redirect:/operator";
					}
					else {
						this.transferService.save(tran);
						this.historyService.save(historyAmount);
						redirectAttributes.addFlashAttribute("Success", "Transfer Success");
						return "redirect:/operator";
					}
				}else if(bankAsal != bankTuju){
					double fee = 6500;
					sisa = tran.getRekening_pengirim().getSaldo() - amount - fee;
					if(sisa < 50000) {
						model.addAttribute("transfer", listTransfer);
						redirectAttributes.addFlashAttribute("Fail", "Saldo kurang dari 50000");
						return "redirect:/operator";
					}
					else {
						this.transferService.save(tran);
						this.historyService.save(historyAmount);
						redirectAttributes.addFlashAttribute("Success", "Transfer Success");
						return "redirect:/operator";
					}
			}

			}
		}
		redirectAttributes.addFlashAttribute("success", "data inserted");
		return "operator";
	}
	@GetMapping("/delete")
	public String delete(@RequestParam("id") Long id) {
		Optional<Transfer> transfer = this.transferService.getTransferById(id);
		if(transfer.isPresent()){
			this.transferService.delete(id);
		}
		return "redirect:/operator";
	}

}
