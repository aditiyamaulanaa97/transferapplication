package com.AditiyaMaulana.TransferApp.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.AditiyaMaulana.TransferApp.entity.Nasabah;
import com.AditiyaMaulana.TransferApp.entity.Provider;
import com.AditiyaMaulana.TransferApp.entity.Rekening;
import com.AditiyaMaulana.TransferApp.service.AdminService;
import com.AditiyaMaulana.TransferApp.service.CustomerSerService;
import com.AditiyaMaulana.TransferApp.service.RekeningService;

@Controller
@RequestMapping("/nasabah")
public class CustomerServiceController {
	@Autowired
	private CustomerSerService customerSerService;
	@Autowired
	private AdminService adminService;
	@Autowired
	private RekeningService rekeningService;

	
	@GetMapping
	public String index(
			@RequestParam(required = false) String keyword,
			@RequestParam(value = "pageNo", defaultValue = "0") int pageNo,
			@RequestParam(value = "pageSize", defaultValue = "1") int pageSize,
			@RequestParam(value = "sortField", defaultValue = "id") String sortField,
			Model model) {
		Page<Nasabah> nasabah;
		List<Rekening> rekenings = this.rekeningService.getAll();
		List<Provider> providers = this.adminService.getAll();
		
		if(keyword == null) {
			nasabah = this.customerSerService.getAllPaginate(pageNo, pageSize, sortField);
		}else {
			nasabah = this.customerSerService.searchNasabah(pageNo, pageSize, sortField, keyword);
		}
		model.addAttribute("nasabahForm", new Nasabah());
		model.addAttribute("rekeningForm", new Rekening());
		model.addAttribute("providers", providers);
		model.addAttribute("keyword", keyword);
		model.addAttribute("rekeningList", rekenings);
		model.addAttribute("page", nasabah);
		
		return "nasabah";
	}
	
	@PostMapping("/save")
	public String save(@Valid @ModelAttribute("nasabahForm") Nasabah nasabah,
			@Valid @ModelAttribute("providers") Provider provider,
			@Valid @ModelAttribute("rekeningForm") Rekening rekening,
			RedirectAttributes redirectAttributes,
			BindingResult result,
			@RequestParam(value = "pageNo", defaultValue = "0") int pageNo,
			@RequestParam(value = "pageSize", defaultValue = "1") int pageSize,
			@RequestParam(value = "sortField", defaultValue = "id") String sortField,
			Model model) {
		if(result.hasErrors()) {
			Page<Nasabah> nasabah1 = this.customerSerService.getAllPaginate(pageNo, pageSize, sortField);
			model.addAttribute("page",nasabah1);
			
			return "nasabah";
		}
		this.rekeningService.addNasabah(provider, rekening, nasabah);
		redirectAttributes.addFlashAttribute("success","data inserted");
		return "redirect:/nasabah";
	}
	

	@GetMapping("/delete")
	public String delete(@RequestParam("id") Long id) {
			this.rekeningService.delete(id);
		return "redirect:/nasabah";
	}
}
