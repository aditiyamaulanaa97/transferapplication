package com.AditiyaMaulana.TransferApp.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.AditiyaMaulana.TransferApp.entity.Rekening;
import com.AditiyaMaulana.TransferApp.service.RekeningService;

@Controller
@RequestMapping("/rekening")
public class RekeningController {
	@Autowired
	private RekeningService rekeningService;

	 @GetMapping public String indexHtml(Model model) { 
	 List<Rekening> addreks = this.rekeningService.getAll(); 
	 model.addAttribute("addreks", addreks);
	 model.addAttribute("addRekForm", new Rekening()); 
	 return "rekening"; 
	 }
	

	//@GetMapping
	public String index(@RequestParam(value = "pageNo", defaultValue = "0") int pageNo,
			@RequestParam(value = "pageSize", defaultValue = "5") int pageSize,
			@RequestParam(value = "sortField", defaultValue = "id") String sortField,
			@RequestParam(value = "keyword", defaultValue = "") String keyword, Model model) {

		Page<Rekening> listRekening = this.rekeningService.getAllPaginate(pageNo, pageSize, sortField, keyword);
		model.addAttribute("page", listRekening);
		model.addAttribute("addTransfer", new Rekening());
		return "rekening";
	}

	@PostMapping("/save")
	public String save(@Valid @ModelAttribute("addRekForm") Rekening rekening, BindingResult result,
			RedirectAttributes redirectAttributes, @RequestParam(value = "pageNo", defaultValue = "0") int pageNo,
			@RequestParam(value = "pageSize", defaultValue = "10") int pageSize,
			@RequestParam(value = "sortField", defaultValue = "id") String sortField,
			@RequestParam(value = "keyword", defaultValue = "") String keyword, Model model) {

		if (result.hasErrors()) {
			Page<Rekening> rekenings = this.rekeningService.getAllPaginate(pageNo, pageSize, sortField, keyword);
			model.addAttribute("page", rekenings);
			return "rekening";
		}
		this.rekeningService.save(rekening);
		redirectAttributes.addFlashAttribute("success", "data inserted");
		return "redirect:/nasabah";

	}

	@GetMapping("/delete")
	public String delete(@RequestParam("id") Long id) {
			this.rekeningService.delete(id);
		return "redirect:/rekening";
	}
}

