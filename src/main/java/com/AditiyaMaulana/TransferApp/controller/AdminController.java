package com.AditiyaMaulana.TransferApp.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.AditiyaMaulana.TransferApp.entity.Provider;
import com.AditiyaMaulana.TransferApp.service.AdminService;

@Controller
@RequestMapping("/provider")
public class AdminController {
	@Autowired
	private AdminService adminService;
	
	@GetMapping
	public String indexHtml(Model model) {
		List<Provider> providers = this.adminService.getAll();
		model.addAttribute("providers", providers);
		model.addAttribute("providerForm", new Provider());
		return "provider";
	}
	// @GetMapping
		public String index(@RequestParam(value = "pageNo", defaultValue = "0") int pageNo,
				@RequestParam (value = "pageSize", defaultValue = "5") int pageSize,
				@RequestParam(value = "sortField", defaultValue = "id") String sortField,
				@RequestParam(value="keyword", defaultValue = "") String keyword,	
				Model model) {
			
			Page<Provider> listProvider;
			
			if(keyword=="") {
				listProvider = this.adminService.getAllPaginate(pageNo, pageSize, sortField);
			}
			else {
				listProvider = this.adminService.searchProvider(pageNo, pageSize, sortField, keyword);
				System.out.println(listProvider.toString());
			}
		model.addAttribute("providerForm", new Provider());
		model.addAttribute("page", listProvider);
		return "provider";
		}
		
		@PostMapping("/save")
		public String save(@Valid @ModelAttribute("providerForm") Provider provider, 
				BindingResult result, 
				RedirectAttributes redirectAttributes,
				@RequestParam(value = "pageNo", defaultValue = "0") int pageNo,
				@RequestParam (value = "pageSize", defaultValue = "10") int pageSize,
				@RequestParam(value = "sortField", defaultValue = "id") String sortField,
				Model model) {
			
			if(result.hasErrors()) {
				Page<Provider> providers = this.adminService.getAllPaginate(pageNo, pageSize, sortField);
				model.addAttribute("page", providers);
				return "provider";
			}
			this.adminService.save(provider);
			redirectAttributes.addFlashAttribute("success", "data inserted");
			return "redirect:/provider";
			
		}
	
	@GetMapping("/delete")
	public String delete(@RequestParam("id") Long id) {
		Optional<Provider> provider = this.adminService.getProviderId(id);
		if(provider.isPresent()){
			this.adminService.delete(provider.get());
		}
		return "redirect:/provider";
	}
}
