package com.AditiyaMaulana.TransferApp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.AditiyaMaulana.TransferApp.entity.Role;
import com.AditiyaMaulana.TransferApp.entity.User;

public interface RoleRepo extends JpaRepository<Role, Long>{
	List<Role> findRoleByUsers(User user);
}
