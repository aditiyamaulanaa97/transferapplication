package com.AditiyaMaulana.TransferApp.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.AditiyaMaulana.TransferApp.entity.HistoryAmount;
import com.AditiyaMaulana.TransferApp.entity.Rekening;

public interface HistoryRepo extends JpaRepository<HistoryAmount, Long>{
	
	@Query(value = "SELECT * FROM nasabah s where s.first_name iLIKE %:keyword% OR s.last_name iLIKE %:keyword%", nativeQuery = true)
	Page<HistoryAmount>findByKeyword(PageRequest paging, @Param("keyword") String keyword);
}
