package com.AditiyaMaulana.TransferApp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.AditiyaMaulana.TransferApp.entity.User;

public interface UserRepo extends JpaRepository<User, Long>{

	User findByUsername(String username);
}
