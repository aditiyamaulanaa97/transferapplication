package com.AditiyaMaulana.TransferApp.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.AditiyaMaulana.TransferApp.entity.Provider;
import com.AditiyaMaulana.TransferApp.repository.AdminRepo;

@Service
@Transactional
public class AdminServiceImpl implements AdminService{
	@Autowired
	private AdminRepo adminRepo;
	
	@Override
	public List<Provider> getAll() {
		// TODO Auto-generated method stub
		return this.adminRepo.findAll();
	}

	@Override
	public void save(Provider provider) {
		// TODO Auto-generated method stub
		this.adminRepo.save(provider);
	}

	@Override
	public void delete(Provider provider) {
		// TODO Auto-generated method stub
		this.adminRepo.delete(provider);
	}

	@Override
	public Optional<Provider> getProviderId(Long Id) {
		// TODO Auto-generated method stub
		return this.adminRepo.findById(Id);
	}

	@Override
	public Page<Provider> getAllPaginate(int pageNo, int pageSize, String field) {
		// TODO Auto-generated method stub
		PageRequest paging = PageRequest.of(pageNo, pageSize, Sort.by(field).ascending());
		return this.adminRepo.findAll(paging);
	}

	@Override
	public Page<Provider> searchProvider(int pageNo, int pageSize, String sortField, String keyword) {
		// TODO Auto-generated method stub
		PageRequest paging = PageRequest.of(pageNo, pageSize, Sort.by(sortField).ascending());
		return this.adminRepo.findByKeyword(paging, keyword);
	}
	
}
