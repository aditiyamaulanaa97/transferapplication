package com.AditiyaMaulana.TransferApp.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.hibernate.service.internal.ProvidedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.AditiyaMaulana.TransferApp.entity.Nasabah;
import com.AditiyaMaulana.TransferApp.entity.Provider;
import com.AditiyaMaulana.TransferApp.entity.Rekening;
import com.AditiyaMaulana.TransferApp.repository.AdminRepo;
import com.AditiyaMaulana.TransferApp.repository.CustomerServiceRepo;
import com.AditiyaMaulana.TransferApp.repository.RekeningRepo;

@Service
@Transactional
public class RekeningServiceImpl implements RekeningService{
	@Autowired
	private RekeningRepo rekeningRepo;
	
	@Autowired
	private CustomerServiceRepo customerServiceRepo;
	
	
	@Override
	public List<Rekening> getAll() {
		// TODO Auto-generated method stub
		return this.rekeningRepo.findAll();
	}

	@Override
	public void save(Rekening rekening) {
		// TODO Auto-generated method stub
		/*
		 * Provider provider =
		 * this.adminRepo.findBynamaBank(rekening.getProvider().getNamaBank()); Rekening
		 * rekening2 = new Rekening();
		 * rekening2.setNoRekening(rekening2.getNoRekening());
		 * rekening2.setProvider(provider); rekening2.setSaldo(500000.0);
		 * rekening2.setNasabah(rekening.getNasabah());
		 */
		this.rekeningRepo.save(rekening);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		this.rekeningRepo.deleteById(id);
	}

	@Override
	public Optional<Rekening> getRekeningId(Long Id) {
		// TODO Auto-generated method stub
		return this.rekeningRepo.findById(Id);
	}

	@Override
	public Page<Rekening> getAllPaginate(int pageNo, int pageSize, String field, String keyword) {
		// TODO Auto-generated method stub
		PageRequest paging = PageRequest.of(pageNo, pageSize, Sort.by(field).ascending());
		return this.rekeningRepo.findAll(paging);
	}

	@Override
	public Page<Rekening> searchRekening(int pageNo, int pageSize, String sortField, String keyword) {
		// TODO Auto-generated method stub
		PageRequest paging = PageRequest.of(pageNo, pageSize, Sort.by(sortField).ascending());
		return this.rekeningRepo.findByKeyword(paging, keyword);
	}

	@Override
	public Rekening findByNoRekening(String noRekening) {
		// TODO Auto-generated method stub
		return this.rekeningRepo.findByNoRekening(noRekening);
	}

	@Override
	public void addNasabah( Provider provider, Rekening rekening,  Nasabah nasabah) {
		// TODO Auto-generated method stub
		this.customerServiceRepo.save(nasabah);
		rekening.setNasabah(nasabah);
		rekening.setProvider(provider);
		this.rekeningRepo.save(rekening);
	}
}
