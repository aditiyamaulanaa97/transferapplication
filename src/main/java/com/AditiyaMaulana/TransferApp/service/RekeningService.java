package com.AditiyaMaulana.TransferApp.service;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.data.domain.Page;

import com.AditiyaMaulana.TransferApp.entity.Nasabah;
import com.AditiyaMaulana.TransferApp.entity.Provider;
import com.AditiyaMaulana.TransferApp.entity.Rekening;

public interface RekeningService {
	public List<Rekening> getAll();
	public Page<Rekening> getAllPaginate(int pageNo, int pageSize, String field, String keyword);
	public Page<Rekening> searchRekening(int pageNo, int pageSize, String sortField, String keyword);
	public void save(Rekening rekening);
	public void delete(Long id);
	public Optional<Rekening> getRekeningId(Long Id);
	public Rekening findByNoRekening(String noRekening);
	public void addNasabah(@Valid Provider provider, @Valid Rekening rekening, @Valid Nasabah nasabah);
}
