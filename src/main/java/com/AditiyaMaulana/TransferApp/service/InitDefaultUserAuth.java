package com.AditiyaMaulana.TransferApp.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.AditiyaMaulana.TransferApp.entity.Role;
import com.AditiyaMaulana.TransferApp.entity.User;
import com.AditiyaMaulana.TransferApp.repository.RoleRepo;
import com.AditiyaMaulana.TransferApp.repository.UserRepo;


@Service
@Transactional
public class InitDefaultUserAuth {
	@Autowired
	private RoleRepo roleRepo;
	@Autowired
	private UserRepo userRepo;
	
	@PostConstruct
	public void index() {
		//Create Role
		Role roleOperator = new Role();
		Role roleCustomerService = new Role();
		Role roleAdmin = new Role();
		
		roleOperator.setRole("operator");
		roleCustomerService.setRole("customerservice");
		roleAdmin.setRole("admin");
		this.roleRepo.save(roleOperator);
		this.roleRepo.save(roleCustomerService);
		this.roleRepo.save(roleAdmin);
		
		//Create User
		List<Role> listRole = new ArrayList<>();
		listRole.add(roleAdmin);
		
		List<Role> listRole1 = new ArrayList<>();
		listRole1.add(roleOperator);
		
		List<Role> listRole2 = new ArrayList<>();
		listRole2.add(roleCustomerService);
		
		
		User userAdmin = new User();
			userAdmin.setUsername("admin");
			userAdmin.setEmail("maybank@maybank.com");
			userAdmin.setPassword(new BCryptPasswordEncoder().encode("12345678"));
			userAdmin.setRoles(listRole);
		User userOperator = new User();
			userOperator.setUsername("operator");
			userOperator.setEmail("operator@maybank.com");
			userOperator.setPassword(new BCryptPasswordEncoder().encode("12345678"));
			userOperator.setRoles(listRole1);
		User userCustomerService = new User();
			userCustomerService.setUsername("customerservice");
			userCustomerService.setEmail("cs@maybank.com");
			userCustomerService.setPassword(new BCryptPasswordEncoder().encode("12345678"));
			userCustomerService.setRoles(listRole2);
		this.userRepo.save(userAdmin);
		this.userRepo.save(userOperator);
		this.userRepo.save(userCustomerService);
	}
}
