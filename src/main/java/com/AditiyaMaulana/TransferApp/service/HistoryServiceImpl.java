package com.AditiyaMaulana.TransferApp.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.AditiyaMaulana.TransferApp.entity.HistoryAmount;
import com.AditiyaMaulana.TransferApp.repository.HistoryRepo;

@Service
@Transactional
public class HistoryServiceImpl implements HistoryService{

	@Autowired
	private HistoryRepo historyRepo;

	@Override
	public Page<HistoryAmount> getPaginateSearch(int pageNo, int pageSize, String field, String keyword) {
		// TODO Auto-generated method stub
		PageRequest paging = PageRequest.of(pageNo, pageSize, Sort.by(field).ascending());
		
		return this.historyRepo.findByKeyword(paging, keyword);
	}

	@Override
	public Page<HistoryAmount> getAllPaginate(int pageNo, int pageSize, String field) {
		// TODO Auto-generated method stub
		PageRequest paging = PageRequest.of(pageNo, pageSize, Sort.by(field).ascending());
		
		return this.historyRepo.findAll(paging);
	}

	@Override
	public List<HistoryAmount> getAll() {
		// TODO Auto-generated method stub
		return this.historyRepo.findAll();
	}

	@Override
	public void save(HistoryAmount historyAmount) {
		// TODO Auto-generated method stub
		this.historyRepo.save(historyAmount);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		this.historyRepo.deleteById(id);
	}

	@Override
	public void getById(Long id) {
		// TODO Auto-generated method stub
		this.historyRepo.findById(id);
	}

	@Override
	public Optional<HistoryAmount> getRekeningById(Long id) {
		// TODO Auto-generated method stub
		return this.historyRepo.findById(id);
	}
}
