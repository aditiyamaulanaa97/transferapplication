package com.AditiyaMaulana.TransferApp.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.AditiyaMaulana.TransferApp.entity.Nasabah;

@Service
public interface CustomerSerService {
	public List<Nasabah> getAll();
	public Page<Nasabah> getAllPaginate(int pageNo, int pageSize, String field);
	public Page<Nasabah> searchNasabah(int pageNo, int pageSize, String sortField, String keyword);
	public void save(Nasabah nasabah);
	public void delete(Nasabah nasabah);
	public Optional<Nasabah> getNasabahId(Long Id);
}
