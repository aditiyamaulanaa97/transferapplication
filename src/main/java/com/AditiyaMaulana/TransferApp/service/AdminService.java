package com.AditiyaMaulana.TransferApp.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.AditiyaMaulana.TransferApp.entity.Provider;

@Service
public interface AdminService {
	public List<Provider> getAll();
	public Page<Provider> getAllPaginate(int pageNo, int pageSize, String field);
	public Page<Provider> searchProvider(int pageNo, int pageSize, String sortField, String keyword);
	public void save(Provider provider);
	public void delete(Provider provider);
	public Optional<Provider> getProviderId(Long Id);
}
