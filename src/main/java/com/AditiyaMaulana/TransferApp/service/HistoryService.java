package com.AditiyaMaulana.TransferApp.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.AditiyaMaulana.TransferApp.entity.HistoryAmount;

@Service
public interface HistoryService {
	
	public Page<HistoryAmount> getPaginateSearch(int pageNo, int pageSize, String field, String keyword);
	public Page<HistoryAmount> getAllPaginate(int pageNo, int pageSize, String field);
	public List<HistoryAmount> getAll();
	public void save(HistoryAmount historyAmount);
	public void delete(Long id);
	public void getById(Long id);
	public Optional<HistoryAmount> getRekeningById(Long id);
}
