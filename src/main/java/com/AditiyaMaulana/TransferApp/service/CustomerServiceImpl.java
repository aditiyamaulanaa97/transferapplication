package com.AditiyaMaulana.TransferApp.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.AditiyaMaulana.TransferApp.entity.Nasabah;
import com.AditiyaMaulana.TransferApp.entity.Provider;
import com.AditiyaMaulana.TransferApp.repository.AdminRepo;
import com.AditiyaMaulana.TransferApp.repository.CustomerServiceRepo;

@Service
@Transactional
public class CustomerServiceImpl implements CustomerSerService{
	@Autowired
	private CustomerServiceRepo customerServiceRepo;
	
	@Override
	public List<Nasabah> getAll() {
		// TODO Auto-generated method stub
		return this.customerServiceRepo.findAll();
	}

	@Override
	public void save(Nasabah nasabah) {
		// TODO Auto-generated method stub
		this.customerServiceRepo.save(nasabah);
	}

	@Override
	public void delete(Nasabah nasabah) {
		// TODO Auto-generated method stub
		this.customerServiceRepo.delete(nasabah);
	}

	@Override
	public Optional<Nasabah> getNasabahId(Long Id) {
		// TODO Auto-generated method stub
		return this.customerServiceRepo.findById(Id);
	}

	@Override
	public Page<Nasabah> getAllPaginate(int pageNo, int pageSize, String field) {
		// TODO Auto-generated method stub
		PageRequest paging = PageRequest.of(pageNo, pageSize, Sort.by(field).ascending());
		return this.customerServiceRepo.findAll(paging);
	}

	@Override
	public Page<Nasabah> searchNasabah(int pageNo, int pageSize, String sortField, String keyword) {
		// TODO Auto-generated method stub
		PageRequest paging = PageRequest.of(pageNo, pageSize, Sort.by(sortField).ascending());
		return this.customerServiceRepo.findByKeyword(paging, keyword);
	}
	
}
